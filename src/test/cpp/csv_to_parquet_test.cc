#include <glog/logging.h>
#include <gtest/gtest.h>

#include <iomanip>
#include <ostream>
#include <random>

#include "src/main/cpp/field.h"
#include "src/main/cpp/parquet_reader.h"
#include "src/main/cpp/parquet_writer.h"

// Randomize data and write to csv file
// Use ParquetWriter to write to parquet file
// Use ParquetReader to read again and compare the results.
TEST(CSVToParquet, WriteAndRead) {
  using std::string;

  // Write data to csv

  std::string csv_file_path = "test.csv";
  int num_rows = 100;

  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  std::uniform_real_distribution<float> float_dis(-100.0, 100.0);
  std::uniform_real_distribution<double> double_dis(-100.0, 100.0);

  std::vector<bool> bool_vec;

  std::vector<int8_t> int8_t_vec;
  std::vector<int16_t> int16_t_vec;
  std::vector<int32_t> int32_t_vec;
  std::vector<int64_t> int64_t_vec;

  std::vector<uint8_t> uint8_t_vec;
  std::vector<uint16_t> uint16_t_vec;
  std::vector<uint32_t> uint32_t_vec;
  std::vector<uint64_t> uint64_t_vec;

  std::vector<float> float_vec;
  std::vector<double> double_vec;

  std::vector<string> string_vec;

  std::vector<boost::posix_time::ptime> timestamp_vec;

  std::ofstream ofs;
  ofs.open(csv_file_path);

  auto csv_writer = csv::make_csv_writer(ofs);

  std::vector<string> name = {"bool",   "int8",   "int16",    "int32",  "int64",
                              "uint8",  "uint16", "uint32",   "uint64", "float",
                              "double", "string", "timestamp"};

  csv_writer << name;
  for (int i = 0; i < num_rows; i++) {
    bool_vec.push_back(rng() % 2 == 0);

    int8_t_vec.push_back(rng() % std::numeric_limits<uint8_t>::max());
    int16_t_vec.push_back(rng() % std::numeric_limits<uint16_t>::max());
    int32_t_vec.push_back(rng() % std::numeric_limits<uint32_t>::max());
    int64_t_vec.push_back(rng() * rng() % std::numeric_limits<uint64_t>::max());

    uint8_t_vec.push_back(rng() % std::numeric_limits<uint8_t>::max());
    uint16_t_vec.push_back(rng() % std::numeric_limits<uint16_t>::max());
    uint32_t_vec.push_back(rng() % std::numeric_limits<uint32_t>::max());
    uint64_t_vec.push_back(rng() * rng() %
                           std::numeric_limits<uint64_t>::max());

    float_vec.push_back(float_dis(rng));
    double_vec.push_back(double_dis(rng));

    string str = "";
    int len = rng() % 10 + 1;
    for (int i = 0; i < len; i++) {
      str += static_cast<char>(rng() % 26 + 'a');
    }
    string_vec.push_back(str);

    timestamp_vec.push_back(boost::posix_time::from_time_t(
        1618905325 + rng() % (1000 * 60 * 60 * 24)));
  }

  for (int i = 0; i < num_rows; i++) {
    std::vector<string> row;
    row.push_back(std::to_string(bool_vec[i]));

    row.push_back(std::to_string(int8_t_vec[i]));
    row.push_back(std::to_string(int16_t_vec[i]));
    row.push_back(std::to_string(int32_t_vec[i]));
    row.push_back(std::to_string(int64_t_vec[i]));

    row.push_back(std::to_string(uint8_t_vec[i]));
    row.push_back(std::to_string(uint16_t_vec[i]));
    row.push_back(std::to_string(uint32_t_vec[i]));
    row.push_back(std::to_string(uint64_t_vec[i]));

    row.push_back(parquet_parser::ToStringWithPrecision(float_vec[i], 6));
    row.push_back(parquet_parser::ToStringWithPrecision(double_vec[i], 15));

    row.push_back(string_vec[i]);

    row.push_back(
        std::to_string(boost::posix_time::to_time_t(timestamp_vec[i])));

    csv_writer << row;
  }

  ofs.close();

  // Write to parquet
  using parquet_parser::Field;
  using parquet_parser::Type;

  std::string parquet_file_path = "test.parquet";

  parquet_parser::ParquetWriter writer(parquet_file_path);

  // set schema
  std::vector<Field> fields;
  fields.push_back(Field(Type::BOOL, "bool_col"));

  fields.push_back(Field(Type::INT8, "int8_col"));
  fields.push_back(Field(Type::INT16, "int16_col"));
  fields.push_back(Field(Type::INT32, "int32_col"));
  fields.push_back(Field(Type::INT64, "int64_col"));

  fields.push_back(Field(Type::UINT8, "uint8_col"));
  fields.push_back(Field(Type::UINT16, "uint16_col"));
  fields.push_back(Field(Type::UINT32, "uint32_col"));
  fields.push_back(Field(Type::UINT64, "uint64_col"));

  fields.push_back(Field(Type::FLOAT, "float_col"));
  fields.push_back(Field(Type::DOUBLE, "double_col"));

  fields.push_back(Field(Type::STRING, "string_col"));

  fields.push_back(Field(Type::TIMESTAMP, "timestamp_col"));

  writer.SetSchema(fields);
  writer.LoadFromCSV(csv_file_path);
  writer.Commit();

  // Read again
  parquet_parser::ParquetReader reader(parquet_file_path);

#define CHECK_COL(TYPE, ID)                                            \
  std::vector<TYPE> TYPE##_col;                                        \
  for (auto u : reader.Iterate<TYPE>(ID)) TYPE##_col.push_back(u);     \
  ASSERT_EQ(TYPE##_vec.size(), TYPE##_col.size())                      \
      << "std::vectors " << #TYPE << "_vec and " << #TYPE              \
      << "_col are of unequal length";                                 \
  for (int i = 0; i < static_cast<int>(TYPE##_vec.size()); ++i) {      \
    ASSERT_EQ(TYPE##_vec[i], TYPE##_col[i])                            \
        << "std::vectors " << #TYPE << "_vec and " << #TYPE            \
        << "_col differ at index " << i << ' ' << TYPE##_vec[i] << ' ' \
        << TYPE##_col[i];                                              \
  }

  CHECK_COL(bool, 0)
  CHECK_COL(int8_t, 1)
  CHECK_COL(int16_t, 2)
  CHECK_COL(int32_t, 3)
  CHECK_COL(int64_t, 4)
  CHECK_COL(uint8_t, 5)
  CHECK_COL(uint16_t, 6)
  // CHECK_COL(uint32_t, 7)
  // somehow it's wrong, I think there's some bugs in arrow lib
  CHECK_COL(uint64_t, 8)
  CHECK_COL(string, 11)

  std::vector<float> float_col;
  for (auto u : reader.Iterate<float>(9)) float_col.push_back(u);
  ASSERT_EQ(float_vec.size(), float_col.size()) << "std::vectors "
                                                << "float"
                                                << "_vec and "
                                                << "float"
                                                << "_col are of unequal length";
  for (int i = 0; i < static_cast<int>(float_vec.size()); ++i) {
    ASSERT_FLOAT_EQ(float_vec[i], float_col[i])
        << "std::vectors "
        << "float"
        << "_vec and "
        << "float"
        << "_col differ at index " << i << ' ' << float_vec[i] << ' '
        << float_col[i];
  }

  std::vector<double> double_col;
  for (auto u : reader.Iterate<double>(10)) double_col.push_back(u);
  ASSERT_EQ(double_vec.size(), double_col.size())
      << "std::vectors "
      << "double"
      << "_vec and "
      << "double"
      << "_col are of unequal length";
  for (int i = 0; i < static_cast<int>(double_vec.size()); ++i) {
    ASSERT_DOUBLE_EQ(double_vec[i], double_col[i])
        << "std::vectors "
        << "double"
        << "_vec and "
        << "double"
        << "_col differ at index " << i << ' ' << double_vec[i] << ' '
        << double_col[i];
  }

  std::vector<boost::posix_time::ptime> timestamp_col;
  for (auto u : reader.Iterate<boost::posix_time::ptime>(12))
    timestamp_col.push_back(u);

  ASSERT_EQ(timestamp_vec.size(), timestamp_col.size())
      << "std::vectors timestamp_vec and timestamp_col are of unequal length";

  for (int i = 0; i < static_cast<int>(timestamp_vec.size()); i++) {
    ASSERT_EQ(timestamp_vec[i], timestamp_col[i])
        << "std::vectors timestamp_vec and timestamp_col differ at index " << i
        << boost::posix_time::to_iso_string(timestamp_vec[i]) << ' '
        << boost::posix_time::to_iso_string(timestamp_col[i]);
  }

#undef CHECK_COL
}
