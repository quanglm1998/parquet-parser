#include <glog/logging.h>
#include <gtest/gtest.h>

#include <ostream>
#include <random>

#include "src/main/cpp/field.h"
#include "src/main/cpp/parquet_reader.h"
#include "src/main/cpp/parquet_writer.h"

using std::string;

class IterationTest : public testing::Test {
 protected:
  const string kFilePath = "iteration_test.parquet";

  void SetUp() override {
    using parquet_parser::Field;
    using parquet_parser::Type;

    writer = parquet_parser::ParquetWriter(kFilePath);

    // set schema
    std::vector<Field> fields;
    fields.push_back(Field(Type::BOOL, "bool_col"));

    fields.push_back(Field(Type::INT8, "int8_col"));
    fields.push_back(Field(Type::INT16, "int16_col"));
    fields.push_back(Field(Type::INT32, "int32_col"));
    fields.push_back(Field(Type::INT64, "int64_col"));

    fields.push_back(Field(Type::UINT8, "uint8_col"));
    fields.push_back(Field(Type::UINT16, "uint16_col"));
    fields.push_back(Field(Type::UINT32, "uint32_col"));
    fields.push_back(Field(Type::UINT64, "uint64_col"));

    fields.push_back(Field(Type::FLOAT, "float_col"));
    fields.push_back(Field(Type::DOUBLE, "double_col"));

    fields.push_back(Field(Type::STRING, "string_col"));

    fields.push_back(Field(Type::TIMESTAMP, "timestamp_col"));

    writer.SetSchema(fields);

    size_t seed = 69;
    std::mt19937 rng;
    rng.seed(seed);
    std::uniform_real_distribution<float> float_dis(-100.0, 100.0);
    std::uniform_real_distribution<double> double_dis(-100.0, 100.0);

    for (int i = 0; i < 200000; i++) {
      bool_vec.push_back(rng() % 2 == 0);

      int8_t_vec.push_back(rng() % std::numeric_limits<uint8_t>::max());
      int16_t_vec.push_back(rng() % std::numeric_limits<uint16_t>::max());
      int32_t_vec.push_back(rng() % std::numeric_limits<uint32_t>::max());
      int64_t_vec.push_back(rng() * rng() %
                            std::numeric_limits<uint64_t>::max());

      uint8_t_vec.push_back(rng() % std::numeric_limits<uint8_t>::max());
      uint16_t_vec.push_back(rng() % std::numeric_limits<uint16_t>::max());
      uint32_t_vec.push_back(rng() % std::numeric_limits<uint32_t>::max());
      uint64_t_vec.push_back(rng() * rng() %
                             std::numeric_limits<uint64_t>::max());

      float_vec.push_back(float_dis(rng));
      double_vec.push_back(double_dis(rng));

      string str = "";
      int len = rng() % 10 + 1;
      for (int i = 0; i < len; i++) {
        str += static_cast<char>(rng() % 26 + 'a');
      }
      string_vec.push_back(str);

      timestamp_vec.push_back(boost::posix_time::from_time_t(
          1618905325 + rng() % (1000 * 60 * 60 * 24)));
    }

    writer.Insert(0, bool_vec);
    writer.Insert(1, int8_t_vec);
    writer.Insert(2, int16_t_vec);
    writer.Insert(3, int32_t_vec);
    writer.Insert(4, int64_t_vec);
    writer.Insert(5, uint8_t_vec);
    writer.Insert(6, uint16_t_vec);
    writer.Insert(7, uint32_t_vec);
    writer.Insert(8, uint64_t_vec);
    writer.Insert(9, float_vec);
    writer.Insert(10, double_vec);
    writer.Insert(11, string_vec);
    writer.Insert(12, timestamp_vec);

    writer.Commit();

    reader = parquet_parser::ParquetReader(kFilePath);
  }

  std::vector<bool> bool_vec;

  std::vector<int8_t> int8_t_vec;
  std::vector<int16_t> int16_t_vec;
  std::vector<int32_t> int32_t_vec;
  std::vector<int64_t> int64_t_vec;

  std::vector<uint8_t> uint8_t_vec;
  std::vector<uint16_t> uint16_t_vec;
  std::vector<uint32_t> uint32_t_vec;
  std::vector<uint64_t> uint64_t_vec;

  std::vector<float> float_vec;
  std::vector<double> double_vec;

  std::vector<string> string_vec;

  std::vector<boost::posix_time::ptime> timestamp_vec;

  parquet_parser::ParquetReader reader;
  parquet_parser::ParquetWriter writer;
};

TEST_F(IterationTest, WritingTime) {
  // Just do nothing to measure writing time
}

// Tests reader for all types that is supported by default iteration method
TEST_F(IterationTest, DefaultIteration) {
#define CHECK_COL(TYPE, ID)                                        \
  std::vector<TYPE> TYPE##_col;                                    \
  for (auto u : reader.Iterate<TYPE>(ID)) TYPE##_col.push_back(u); \
  ASSERT_EQ(TYPE##_vec.size(), TYPE##_col.size())                  \
      << "std::vectors " << #TYPE << "_vec and " << #TYPE          \
      << "_col are of unequal length";                             \
  for (int i = 0; i < static_cast<int>(TYPE##_vec.size()); ++i) {  \
    ASSERT_EQ(TYPE##_vec[i], TYPE##_col[i])                        \
        << "std::vectors " << #TYPE << "_vec and " << #TYPE        \
        << "_col differ at index " << i << TYPE##_vec[i] << ' '    \
        << TYPE##_col[i];                                          \
  }

  CHECK_COL(bool, 0)
  CHECK_COL(int8_t, 1)
  CHECK_COL(int16_t, 2)
  CHECK_COL(int32_t, 3)
  CHECK_COL(int64_t, 4)
  CHECK_COL(uint8_t, 5)
  CHECK_COL(uint16_t, 6)
  // CHECK_COL(uint32_t, 7)
  // somehow it's wrong, I think there's some bugs in arrow lib
  CHECK_COL(uint64_t, 8)
  CHECK_COL(float, 9)
  CHECK_COL(double, 10)
  CHECK_COL(string, 11)

  std::vector<boost::posix_time::ptime> timestamp_col;
  for (auto u : reader.Iterate<boost::posix_time::ptime>(12))
    timestamp_col.push_back(u);

  ASSERT_EQ(timestamp_vec.size(), timestamp_col.size())
      << "std::vectors timestamp_vec and timestamp_col are of unequal length";

  for (int i = 0; i < static_cast<int>(timestamp_vec.size()); i++) {
    ASSERT_EQ(timestamp_vec[i], timestamp_col[i])
        << "std::vectors timestamp_vec and timestamp_col differ at index " << i
        << boost::posix_time::to_iso_string(timestamp_vec[i]) << ' '
        << boost::posix_time::to_iso_string(timestamp_col[i]);
  }

#undef CHECK_COL
}

// Tests with custom parser on int32 to string
TEST_F(IterationTest, CustomParser) {
  std::vector<int> original_col;
  for (auto value : reader.Iterate<int>(3)) original_col.push_back(value);

  std::vector<string> custom_col;
  for (auto value : reader.Iterate<string, int>(
           3, [](int u) { return "value = " + std::to_string(u); }))
    custom_col.push_back(value);

  ASSERT_EQ(original_col.size(), custom_col.size());
  for (int i = 0; i < static_cast<int>(original_col.size()); i++) {
    string current = "value = " + std::to_string(original_col[i]);
    ASSERT_EQ(current, custom_col[i]);
  }
}

TEST_F(IterationTest, SingleGet) {
  std::vector<int> original_col;
  for (auto value : reader.Iterate<int>(3)) {
    original_col.push_back(value);
    if (original_col.size() == 700) break;
  }
  std::vector<int> get_col;
  for (int i = static_cast<int>(original_col.size()) - 1; i >= 0; i--) {
    get_col.push_back(reader.Get<int>(i, 3));
  }
  std::reverse(get_col.begin(), get_col.end());
  ASSERT_EQ(original_col.size(), get_col.size());
  for (int i = 0; i < static_cast<int>(original_col.size()); i++) {
    ASSERT_EQ(original_col[i], get_col[i]);
  }
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  for (int it = 0; it < 700; it++) {
    auto u = rng() % original_col.size();
    ASSERT_EQ(original_col[u], reader.Get<int>(u, 3));
  }
}

TEST_F(IterationTest, MultipleColumns) {
  std::vector<std::vector<std::string>> table;
  for (auto row : reader.IterateRows()) {
    table.push_back(row);
  }

  std::vector<int> columns_vec = {1, 3, 5};
  int cnt = 0;
  for (auto row : reader.Iterate(columns_vec)) {
    for (int i = 0; i < static_cast<int>(row.size()); i++) {
      ASSERT_EQ(row[i], table[cnt][columns_vec[i]]);
    }
    cnt++;
  }
}

TEST_F(IterationTest, GetInRow) {
  std::vector<boost::posix_time::ptime> original_col;
  for (auto value : reader.Iterate<boost::posix_time::ptime>(12)) {
    original_col.push_back(value);
    if (original_col.size() == 100) break;
  }
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  for (int it = 0; it < 10; ++it) {
    std::vector<int> columns_vec = {12};
    auto row_it =
        parquet_parser::RowIterator(columns_vec, 0, reader.file_reader());
    int num = rng() % original_col.size();
    for (int i = 0; i < num; i++) ++row_it;
    ASSERT_EQ(original_col[num], row_it.Get<boost::posix_time::ptime>(12));
  }
}

// Tests for null elements
TEST(ExternalFile, Optional) {
  parquet_parser::ParquetReader reader =
      parquet_parser::ParquetReader("data/userdata1.parquet");

  std::vector<string> col;
  for (auto val : reader.Iterate<string>(12, "null_value")) col.push_back(val);

  std::vector<std::optional<string>> optional_col;
  for (auto u : reader.IterateOptional<string>(12)) optional_col.push_back(u);

  ASSERT_EQ(col.size(), optional_col.size());
  for (int i = 0; i < static_cast<int>(col.size()); i++) {
    auto u = optional_col[i];
    if (u)
      ASSERT_EQ(*u, col[i]);
    else
      ASSERT_EQ("null_value", col[i]);
  }
}

TEST(ExternalFile, IterationWithExternalFile) {
  for (int test = 1; test <= 5; test++) {
    parquet_parser::ParquetReader reader = parquet_parser::ParquetReader(
        "data/userdata" + std::to_string(test) + ".parquet");
    int cnt = 0;
    for (auto u : reader.Iterate<int>(1, -1)) {
      cnt++;
      if (u == -1) continue;
      ASSERT_EQ(u, cnt);
    }
  }
}
