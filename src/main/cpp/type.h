#pragma once

#include <arrow/api.h>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace parquet_parser {

// Defines appropriate arrow ARRAY types for all supported types.
template <typename T>
struct cpp_type_to_arrow_array_type {};

template <>
struct cpp_type_to_arrow_array_type<bool> {
  using type = arrow::BooleanArray;
};

template <>
struct cpp_type_to_arrow_array_type<int8_t> {
  using type = arrow::Int8Array;
};

template <>
struct cpp_type_to_arrow_array_type<int16_t> {
  using type = arrow::Int16Array;
};

template <>
struct cpp_type_to_arrow_array_type<int32_t> {
  using type = arrow::Int32Array;
};

template <>
struct cpp_type_to_arrow_array_type<int64_t> {
  using type = arrow::Int64Array;
};

template <>
struct cpp_type_to_arrow_array_type<uint8_t> {
  using type = arrow::UInt8Array;
};

template <>
struct cpp_type_to_arrow_array_type<uint16_t> {
  using type = arrow::UInt16Array;
};

template <>
struct cpp_type_to_arrow_array_type<uint32_t> {
  using type = arrow::UInt32Array;
};

template <>
struct cpp_type_to_arrow_array_type<uint64_t> {
  using type = arrow::UInt64Array;
};

template <>
struct cpp_type_to_arrow_array_type<float> {
  using type = arrow::FloatArray;
};

template <>
struct cpp_type_to_arrow_array_type<double> {
  using type = arrow::DoubleArray;
};

template <>
struct cpp_type_to_arrow_array_type<std::string> {
  using type = arrow::StringArray;
};

template <>
struct cpp_type_to_arrow_array_type<boost::posix_time::ptime> {
  using type = arrow::TimestampArray;
};

// Defines appropriate arrow BUILDER types for all supported types.
template <typename T>
using cpp_type_to_arrow_array_type_t =
    typename cpp_type_to_arrow_array_type<T>::type;


template <typename T>
struct cpp_type_to_arrow_builder_type {};

template <>
struct cpp_type_to_arrow_builder_type<bool> {
  using type = arrow::BooleanBuilder;
};

template <>
struct cpp_type_to_arrow_builder_type<int8_t> {
  using type = arrow::Int8Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<int16_t> {
  using type = arrow::Int16Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<int32_t> {
  using type = arrow::Int32Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<int64_t> {
  using type = arrow::Int64Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<uint8_t> {
  using type = arrow::UInt8Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<uint16_t> {
  using type = arrow::UInt16Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<uint32_t> {
  using type = arrow::UInt32Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<uint64_t> {
  using type = arrow::UInt64Builder;
};

template <>
struct cpp_type_to_arrow_builder_type<float> {
  using type = arrow::FloatBuilder;
};

template <>
struct cpp_type_to_arrow_builder_type<double> {
  using type = arrow::DoubleBuilder;
};

template <>
struct cpp_type_to_arrow_builder_type<std::string> {
  using type = arrow::StringBuilder;
};

template <>
struct cpp_type_to_arrow_builder_type<boost::posix_time::ptime> {
  using type = arrow::TimestampBuilder;
};

template <typename T>
using cpp_type_to_arrow_builder_type_t =
    typename cpp_type_to_arrow_builder_type<T>::type;

// All supported types
enum class Type {
  BOOL,
  INT8,
  INT16,
  INT32,
  INT64,
  UINT8,
  UINT16,
  UINT32,
  UINT64,
  FLOAT,
  DOUBLE,
  STRING,
  TIMESTAMP,
};

}  // namespace parquet_parser
