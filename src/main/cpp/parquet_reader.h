#pragma once

#include <parquet/arrow/reader.h>

#include "src/main/cpp/column_iterator.h"
#include "src/main/cpp/iterator_range.h"
#include "src/main/cpp/row_iterator.h"

namespace parquet_parser {

// Parquet Reader allows users to iterate through cells in a parquet file.
//
// Reader works with a row group at a time.
//   - When using column iterator, it stores all elements of a column in a row
//   group in memory.
//   - When using row iterator, it stores the whole information of a row group
//   in memory
class ParquetReader {
 public:
  ParquetReader() = default;

  explicit ParquetReader(const std::string& file_path);

  template <typename T>
  T Get(int row_id, int column_id,
        T default_value = default_value<T>::value()) const {
    static ColumnIterator<T> last_it;
    static int last_column_id = -1;
    static int last_row_id = -1;

    ColumnIterator<T> it;
    int row = 0;

    if (row_id >= last_row_id && column_id == last_column_id) {
      it = last_it;
      row = last_row_id;
    } else {
      it = ColumnIterator<T>(0, column_id, 0, 0, file_reader_.get(),
                             default_value);
    }

    it += row_id - row;

    last_row_id = row_id;
    last_column_id = column_id;
    last_it = it;
    return *it;
  }

  template <typename T>
  IteratorRange<ColumnIterator<T>> Iterate(
      int column_id, T default_value = default_value<T>::value()) const {
    ColumnIterator<T> begin_iterator(0, column_id, 0, 0, file_reader_.get(),
                                     default_value);
    ColumnIterator<T> end_iterator(file_reader_->num_row_groups(), column_id, 0,
                                   0, file_reader_.get(), default_value);
    return IteratorRange<ColumnIterator<T>>(begin_iterator, end_iterator);
  }

  IteratorRange<RowIterator> Iterate(std::vector<int> columns_vec) const;

  // column with column_id has type `U` originally
  // returns `T` instead by custom function func(U) -> T
  template <typename T, typename U>
  IteratorRange<ColumnIteratorCustomParser<T, U>> Iterate(
      int column_id, std::function<T(U)> func) const {
    ColumnIteratorCustomParser<T, U> begin_iterator(0, column_id, 0, 0,
                                                    file_reader_.get(), func);
    ColumnIteratorCustomParser<T, U> end_iterator(
        file_reader_->num_row_groups(), column_id, 0, 0, file_reader_.get(),
        func);
    return IteratorRange<ColumnIteratorCustomParser<T, U>>(begin_iterator,
                                                           end_iterator);
  }

  template <typename T>
  IteratorRange<ColumnIteratorOptional<T>> IterateOptional(
      int column_id) const {
    ColumnIteratorOptional<T> begin_iterator(0, column_id, 0, 0,
                                             file_reader_.get());
    ColumnIteratorOptional<T> end_iterator(file_reader_->num_row_groups(),
                                           column_id, 0, 0, file_reader_.get());
    return IteratorRange<ColumnIteratorOptional<T>>(begin_iterator,
                                                    end_iterator);
  }

  parquet::arrow::FileReader* file_reader() const { return file_reader_.get(); }

  IteratorRange<RowIterator> IterateRows() const;

  std::vector<std::string> GetSchemaAsString() const;

  std::shared_ptr<arrow::Schema> GetSchema() const;

 private:
  std::unique_ptr<parquet::arrow::FileReader> file_reader_;
};

}  // namespace parquet_parser
