#pragma once

#include <arrow/array/array_binary.h>
#include <arrow/array/array_primitive.h>
#include <arrow/chunked_array.h>
#include <glog/logging.h>
#include <parquet/arrow/reader.h>

#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "src/main/cpp/exceptions.h"
#include "src/main/cpp/type.h"
#include "src/main/cpp/utils.h"

namespace parquet_parser {

template <typename T>
struct default_value {
  static T value() { return 0; }
};

template <>
struct default_value<std::string> {
  static std::string value() { return ""; }
};

template <>
struct default_value<boost::posix_time::ptime> {
  static boost::posix_time::ptime value() {
    return boost::posix_time::from_time_t(0);
  }
};

// ColumnIterator stores all the information needed for iterating through a
// column in a table. The whole column in a row group has to be stored IN MEMORY
// at a time. Be careful when the row group size is too large.
template <typename T>
class ColumnIterator {
 public:
  using ArrowArrayType = cpp_type_to_arrow_array_type_t<T>;

  ColumnIterator() = default;

  ColumnIterator(int32_t row_group_id, int32_t column_id, int32_t chunk_id,
                 int32_t id_in_array, parquet::arrow::FileReader* reader,
                 T default_value = default_value<T>::value())
      : id_in_array_(id_in_array),
        row_group_id_(row_group_id),
        column_id_(column_id),
        chunk_id_(chunk_id),
        reader_(reader),
        default_value_(default_value) {
    if (row_group_id_ < reader_->num_row_groups()) {
      PARQUET_THROW_NOT_OK(reader_->RowGroup(row_group_id_)
                               ->Column(column_id_)
                               ->Read(&chunked_array_));
      array_ = chunked_array_->chunk(chunk_id_);
    } else {
      chunked_array_ = nullptr;
      array_ = nullptr;
    }
  }

  // Pre-increment iterator
  ColumnIterator& operator++() {
    if (row_group_id_ >= reader_->num_row_groups()) {
      return *this;
    }
    id_in_array_++;
    if (id_in_array_ >= array_->length()) {
      chunk_id_++;
      id_in_array_ = 0;
      if (chunk_id_ >= chunked_array_->num_chunks()) {
        row_group_id_++;
        chunk_id_ = 0;
        if (row_group_id_ < reader_->num_row_groups()) {
          PARQUET_THROW_NOT_OK(reader_->RowGroup(row_group_id_)
                                   ->Column(column_id_)
                                   ->Read(&chunked_array_));
          array_ = chunked_array_->chunk(chunk_id_);
        } else {
          chunked_array_ = nullptr;
          array_ = nullptr;
        }
      } else {
        array_ = chunked_array_->chunk(chunk_id_);
      }
    }
    return *this;
  }

  // Post-increment iterator
  ColumnIterator operator++(int) {
    auto temp = *this;
    operator++();
    return temp;
  }

  ColumnIterator operator+=(uint64_t n) {
    while (n) {
      ++(*this);
      n--;
    }
    return *this;
  }

  // Crash immediately when 2 column iterators don't share the same reader or
  // column id. It makes no sense when we try to compare these two.
  bool operator==(const ColumnIterator& rhs) const {
    CHECK(reader_ == rhs.reader_ && column_id_ == rhs.column_id_);
    return row_group_id_ == rhs.row_group_id_ && chunk_id_ == rhs.chunk_id_ &&
           id_in_array_ == rhs.id_in_array_;
  }

  bool operator!=(const ColumnIterator& rhs) const { return !(*this == rhs); }

  T operator*() {
    if (array_->IsNull(id_in_array_)) return default_value_;
    if constexpr (std::is_same_v<T, boost::posix_time::ptime>) {
      auto casted_array = std::static_pointer_cast<ArrowArrayType>(array_);
      auto value = casted_array->Value(id_in_array_);
      return FromInt64(value);
    } else if constexpr (std::is_same_v<T, std::string>) {
      auto casted_array = std::static_pointer_cast<ArrowArrayType>(array_);
      return boost::algorithm::trim_copy(casted_array->GetString(id_in_array_));
    } else {
      auto casted_array = std::static_pointer_cast<ArrowArrayType>(array_);
      return casted_array->Value(id_in_array_);
    }
  }

  // Checks if this iterator has reached the end of the table.
  bool IsEnd() const { return row_group_id_ >= reader_->num_row_groups(); }

  // Acts almost the same as operator *, but it converts all type to std::string
  // instead for row iteration
  std::string GetString() const {
    if (IsEnd() || array_->IsNull(id_in_array_)) return "";

    if (array_->type_id() == arrow::Type::BOOL) {
      auto casted_array = std::static_pointer_cast<arrow::BooleanArray>(array_);
      return casted_array->Value(id_in_array_) ? "true" : "false";
    }

    if (array_->type_id() == arrow::Type::INT8) {
      auto casted_array = std::static_pointer_cast<arrow::Int8Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::INT16) {
      auto casted_array = std::static_pointer_cast<arrow::Int16Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::INT32) {
      auto casted_array = std::static_pointer_cast<arrow::Int32Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::INT64) {
      auto casted_array = std::static_pointer_cast<arrow::Int64Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::UINT8) {
      auto casted_array = std::static_pointer_cast<arrow::UInt8Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::UINT16) {
      auto casted_array = std::static_pointer_cast<arrow::UInt16Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::UINT32) {
      auto casted_array = std::static_pointer_cast<arrow::UInt32Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::UINT64) {
      auto casted_array = std::static_pointer_cast<arrow::UInt64Array>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::FLOAT) {
      auto casted_array = std::static_pointer_cast<arrow::FloatArray>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::DOUBLE) {
      auto casted_array = std::static_pointer_cast<arrow::DoubleArray>(array_);
      return std::to_string(casted_array->Value(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::STRING) {
      auto casted_array = std::static_pointer_cast<arrow::StringArray>(array_);
      return boost::algorithm::trim_copy(casted_array->GetString(id_in_array_));
    }

    if (array_->type_id() == arrow::Type::TIMESTAMP) {
      auto casted_array =
          std::static_pointer_cast<arrow::TimestampArray>(array_);
      auto value = casted_array->Value(id_in_array_);
      auto time = FromInt64(value);
      return boost::posix_time::to_iso_string(time);
    }

    THROW() << "Type " << std::to_string(array_->type_id())
            << " hasn't been supported!";
    return "";
  }

 protected:
  int32_t id_in_array_;
  std::shared_ptr<arrow::Array> array_;

 private:
  int32_t row_group_id_;
  int32_t column_id_;
  int32_t chunk_id_;
  std::shared_ptr<arrow::ChunkedArray> chunked_array_;
  parquet::arrow::FileReader* reader_;
  T default_value_;
};

template <typename T>
class ColumnIteratorOptional : public ColumnIterator<T> {
 public:
  using ArrowArrayType = cpp_type_to_arrow_array_type_t<T>;
  using ColumnIterator<T>::id_in_array_;
  using ColumnIterator<T>::array_;

  ColumnIteratorOptional() = delete;

  ColumnIteratorOptional(int32_t row_group_id, int32_t column_id,
                         int32_t chunk_id, int32_t id_in_array,
                         parquet::arrow::FileReader* reader)
      : ColumnIterator<T>(row_group_id, column_id, chunk_id, id_in_array,
                          reader) {}

  std::optional<T> operator*() {
    if (array_->IsNull(id_in_array_)) return std::nullopt;
    return ColumnIterator<T>::operator*();
  }
};

template <typename T, typename U>
class ColumnIteratorCustomParser : public ColumnIterator<U> {
 public:
  ColumnIteratorCustomParser() = delete;

  ColumnIteratorCustomParser(int32_t row_group_id, int32_t column_id,
                             int32_t chunk_id, int32_t id_in_array,
                             parquet::arrow::FileReader* reader,
                             std::function<T(U)> func)
      : ColumnIterator<U>(row_group_id, column_id, chunk_id, id_in_array,
                          reader),
        func_(func) {}

  T operator*() {
    U raw_value = ColumnIterator<U>::operator*();
    return func_(raw_value);
  }

 private:
  std::function<T(U)> func_;
};

}  // namespace parquet_parser
