// Generic THROW() exceptions

#pragma once
#include <boost/core/noncopyable.hpp>

namespace parquet_parser {

class ExceptionStreamBase {};

class ExceptionStream : public ExceptionStreamBase, public boost::noncopyable {
 public:
  ~ExceptionStream() noexcept(false) {
    std::string line = std::string("[") + std::string(__FILE__) + ":" +
                       std::to_string(__LINE__) + "] ";
    throw std::runtime_error(line + oss_.str());
  }

  template <typename Arg>
  ExceptionStream &operator<<(const Arg &arg) {
    oss_ << arg;
    return *this;
  }

 private:
  std::ostringstream oss_;
};

class ExceptionStreamVoidify {
 public:
  ExceptionStreamVoidify() {}
  void operator&(const ExceptionStreamBase &) {}
};

#define THROW() THROW_IF(true)

#define THROW_IF(cond) \
  !(cond) ? (void)0 : ExceptionStreamVoidify() & ExceptionStream()

#define EXTERNAL_THROW_IF(cond)                        \
  !(cond) ? (void)0                                    \
          : parquet_parser::ExceptionStreamVoidify() & \
                parquet_parser::ExceptionStream()

}  // namespace parquet_parser
