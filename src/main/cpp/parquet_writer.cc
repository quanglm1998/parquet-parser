#include "src/main/cpp/parquet_writer.h"

namespace parquet_parser {

ParquetWriter::ParquetWriter(const std::string &file_path,
                             int64_t row_group_size) {
  row_group_size_ = row_group_size;
  PARQUET_ASSIGN_OR_THROW(output_file_,
                          arrow::io::FileOutputStream::Open(file_path));
}

void ParquetWriter::SetSchema(const std::vector<Field> &fields) {
  std::vector<std::shared_ptr<arrow::Field>> new_fields;
  for (const auto &field : fields) {
    switch (field.type) {
      case Type::BOOL:
        new_fields.push_back(arrow::field(field.name, arrow::boolean()));
        break;
      case Type::INT8:
        new_fields.push_back(arrow::field(field.name, arrow::int8()));
        break;
      case Type::INT16:
        new_fields.push_back(arrow::field(field.name, arrow::int16()));
        break;
      case Type::INT32:
        new_fields.push_back(arrow::field(field.name, arrow::int32()));
        break;
      case Type::INT64:
        new_fields.push_back(arrow::field(field.name, arrow::int64()));
        break;
      case Type::UINT8:
        new_fields.push_back(arrow::field(field.name, arrow::uint8()));
        break;
      case Type::UINT16:
        new_fields.push_back(arrow::field(field.name, arrow::uint16()));
        break;
      case Type::UINT32:
        new_fields.push_back(arrow::field(field.name, arrow::uint32()));
        break;
      case Type::UINT64:
        new_fields.push_back(arrow::field(field.name, arrow::uint64()));
        break;
      case Type::FLOAT:
        new_fields.push_back(arrow::field(field.name, arrow::float32()));
        break;
      case Type::DOUBLE:
        new_fields.push_back(arrow::field(field.name, arrow::float64()));
        break;
      case Type::STRING:
        new_fields.push_back(arrow::field(field.name, arrow::utf8()));
        break;
      case Type::TIMESTAMP:
        new_fields.push_back(
            arrow::field(field.name, arrow::timestamp(arrow::TimeUnit::MICRO)));
        break;

      default:
        break;
    }
  }
  schema_ = arrow::schema(new_fields);
  columns_.resize(fields.size());
}

void ParquetWriter::LoadFromCSV(const std::string &csv_file_path) {
  // read data from csv file
  csv::CSVReader reader(csv_file_path);
  std::vector<std::vector<std::string>> csv_table;
  for (csv::CSVRow &row : reader) {
    csv_table.push_back(std::vector<std::string>());
    for (csv::CSVField &field : row) {
      // By default, get<>() produces a std::string.
      csv_table.back().push_back(field.get<>());
    }
  }

  THROW_IF(csv_table.empty()) << "CSV file is empty!";
  int num_columns = csv_table[0].size();
  for (int i = 1; i < static_cast<int>(csv_table.size()); i++) {
    THROW_IF(num_columns != static_cast<int>(csv_table[i].size()))
        << "Number of columns in each row is different";
  }

  auto fields = schema_->fields();

  for (int i = 0; i < num_columns; i++) {
    auto data_type_id = fields[i]->type()->id();

#define ADD_COLUMN(CLASS, BUILDER, TYPE)                           \
  if (data_type_id == arrow::Type::CLASS) {                        \
    arrow::BUILDER builder;                                        \
    for (int j = 0; j < static_cast<int>(csv_table.size()); j++) { \
      builder.Append(FromString<TYPE>(csv_table[j][i]));           \
    }                                                              \
    PARQUET_THROW_NOT_OK(builder.Finish(&columns_[i]));            \
  }

    ADD_COLUMN(BOOL, BooleanBuilder, bool);
    ADD_COLUMN(INT8, Int8Builder, int8_t);
    ADD_COLUMN(INT16, Int16Builder, int16_t);
    ADD_COLUMN(INT32, Int32Builder, int32_t);
    ADD_COLUMN(INT64, Int64Builder, int64_t);
    ADD_COLUMN(UINT8, UInt8Builder, uint8_t);
    ADD_COLUMN(UINT16, UInt16Builder, uint16_t);
    ADD_COLUMN(UINT32, UInt32Builder, uint32_t);
    ADD_COLUMN(UINT64, UInt64Builder, uint64_t);
    ADD_COLUMN(FLOAT, FloatBuilder, float);
    ADD_COLUMN(DOUBLE, DoubleBuilder, double);
    ADD_COLUMN(STRING, StringBuilder, std::string);

    // a special case for TIMESTAMP because its builder requires int64_t instead
    if (data_type_id == arrow::Type::TIMESTAMP) {
      arrow::TimestampBuilder builder(arrow::timestamp(arrow::TimeUnit::MICRO),
                                      arrow::default_memory_pool());

      for (int j = 0; j < static_cast<int>(csv_table.size()); j++) {
        builder.Append(FromString<int64_t>(csv_table[j][i]));
      }

      PARQUET_THROW_NOT_OK(builder.Finish(&columns_[i]));
    }
  }

#undef ADD_COLUMN
}

void ParquetWriter::Commit() {
  auto table = arrow::Table::Make(schema_, columns_);
  PARQUET_THROW_NOT_OK(parquet::arrow::WriteTable(
      *table, arrow::default_memory_pool(), output_file_, row_group_size_));
}

}  // namespace parquet_parser
