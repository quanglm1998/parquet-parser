#pragma once

#include "src/main/cpp/type.h"

namespace parquet_parser {

struct Field {
  Type type;
  std::string name;

  Field(Type type, std::string name) : type(type), name(name) {}
};

}  // namespace parquet_parser
