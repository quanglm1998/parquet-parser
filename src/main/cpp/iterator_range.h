#pragma once

namespace parquet_parser {

template <typename Iterator>
class IteratorRange {
 public:
  IteratorRange(Iterator begin, Iterator end) : begin_(begin), end_(end) {}

  const Iterator begin() const { return begin_; }

  const Iterator end() const { return end_; }

 private:
  Iterator begin_;
  Iterator end_;
};

}  // namespace parquet_parser
