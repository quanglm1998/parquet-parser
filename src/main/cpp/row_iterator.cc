#include "src/main/cpp/row_iterator.h"

namespace parquet_parser {

RowIterator::RowIterator(int32_t row_group_id,
                         parquet::arrow::FileReader *reader)
    : row_group_id_(row_group_id), reader_(reader) {
  std::shared_ptr<arrow::Schema> schema;
  PARQUET_THROW_NOT_OK(reader_->GetSchema(&schema));
  real_column_id_.clear();
  real_column_id_.reserve(schema->num_fields());
  column_iterators_.clear();
  for (int i = 0; i < schema->num_fields(); i++) {
    real_column_id_[i] = i;
    column_iterators_.push_back(
        ColumnIterator<std::string>(row_group_id_, i, 0, 0, reader_));
  }
}

RowIterator::RowIterator(std::vector<int32_t> columns_vec, int32_t row_group_id,
                         parquet::arrow::FileReader *reader)
    : row_group_id_(row_group_id), reader_(reader) {
  real_column_id_.clear();
  real_column_id_.reserve(columns_vec.size());
  column_iterators_.clear();
  for (auto id : columns_vec) {
    real_column_id_[id] = column_iterators_.size();
    column_iterators_.push_back(
        ColumnIterator<std::string>(row_group_id_, id, 0, 0, reader_));
  }
}

RowIterator &RowIterator::operator++() { /**< Pre-increment iterator */
  for (auto &column_iterator : column_iterators_) {
    column_iterator++;
  }
  return *this;
}

RowIterator RowIterator::operator++(int) { /**< Post-increment iterator */
  auto temp = *this;
  operator++();
  return temp;
}

bool RowIterator::operator==(const RowIterator &rhs) const {
  CHECK(column_iterators_.size() == rhs.column_iterators().size());
  for (int i = 0; i < static_cast<int>(column_iterators_.size()); ++i) {
    if (column_iterators_[i] != rhs.column_iterators_[i]) {
      return false;
    }
  }
  return true;
}

bool RowIterator::operator!=(const RowIterator &rhs) const {
  return !(*this == rhs);
}

std::vector<std::string> RowIterator::operator*() const {
  std::vector<std::string> res;
  for (const auto &column_iterator : column_iterators_) {
    res.push_back(column_iterator.GetString());
  }
  return res;
}

}  // namespace parquet_parser
