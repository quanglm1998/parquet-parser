#pragma once

#include <sstream>

namespace parquet_parser {

// convert std::string to all other types
template <typename T>
T FromString(const std::string &s) {
  T value;
  std::istringstream iss(s);
  iss >> value;
  return value;
}

// Template doesn't work for `int8_t` and `uint8_t`. They are treated as char
// when using stream.
template <>
int8_t FromString<int8_t>(const std::string &s) {
  int16_t value;
  std::istringstream iss(s);
  iss >> value;
  return static_cast<int8_t>(value);
}

template <>
uint8_t FromString<uint8_t>(const std::string &s) {
  uint16_t value;
  std::istringstream iss(s);
  iss >> value;
  return static_cast<uint8_t>(value);
}

template <>
std::string FromString<std::string>(const std::string &s) {
  return s;
}

// convert from ptime's "simple string" format to ptime
template <>
boost::posix_time::ptime FromString<boost::posix_time::ptime>(
    const std::string &s) {
  return boost::posix_time::from_iso_string(s);
}

// Converts 64-bit unix timestamp to `boost::posix_time::ptime`
static boost::posix_time::ptime FromInt64(int64_t value) {
  int32_t divs = 1;
  while (value / divs > std::numeric_limits<int32_t>::max()) {
    divs *= 1000;
  }
  return boost::posix_time::from_time_t(value / divs) +
         boost::posix_time::microseconds(value % divs * 1000000 / divs);
}

// default std::to_string defaults to 6 decimal places when given an input value
// of type float or double
template <typename T>
static std::string ToStringWithPrecision(const T value, const int n = 6) {
  std::ostringstream out;
  out.precision(n);
  out << std::fixed << value;
  return out.str();
}

}  // namespace parquet_parser
