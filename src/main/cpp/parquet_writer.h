#pragma once

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <parquet/arrow/writer.h>

#include <boost/date_time/posix_time/posix_time.hpp>

#include "src/main/cpp/exceptions.h"
#include "src/main/cpp/field.h"
#include "src/main/cpp/utils.h"
#include "third_party/csv.hpp"

namespace parquet_parser {

// Parquet Reader allows users to write to parquet files.
//
// All of the cells in the table need to be stored in memory in order to be
// written to file.
// To write to file, we need to go through some step:
//   - `SetSchema` lets writer know the parquet table's description (how many
//   columns there are, name and type of each columns).
//   - `Insert` a std::vector to a particular column. All columns should have
//   the same number of rows, otherwise the program will crash.
//   - `Commit` writes all information to file.
class ParquetWriter {
 public:
  static const int64_t kDefaultRowGroupSize = 8 * 1024 * 1024;

  ParquetWriter() = default;

  ParquetWriter(const std::string &file_path,
                int64_t row_group_size = kDefaultRowGroupSize);

  void SetSchema(const std::vector<Field> &fields);

  // Set a table's column with a vector with type corresponding to that column's
  // type
  template <typename T>
  void Insert(int column_id, const std::vector<T> &column) {
    using BuilderType = cpp_type_to_arrow_builder_type_t<T>;
    std::shared_ptr<arrow::Array> array;

    if constexpr (std::is_same_v<T, boost::posix_time::ptime>) {
      BuilderType builder(arrow::timestamp(arrow::TimeUnit::MICRO),
                          arrow::default_memory_pool());

      for (auto u : column) {
        auto time_u = boost::posix_time::to_time_t(u);
        builder.Append((int64_t)time_u);
      }

      PARQUET_THROW_NOT_OK(builder.Finish(&array));
    } else {
      BuilderType builder;
      builder.AppendValues(column);
      PARQUET_THROW_NOT_OK(builder.Finish(&array));
    }
    columns_[column_id] = array;
  }

  // Load table from CSV
  void LoadFromCSV(const std::string &csv_file_path);

  // Actually write to file
  void Commit();

  std::shared_ptr<arrow::Schema> schema() const { return schema_; }

  std::shared_ptr<arrow::io::FileOutputStream> output_file() const {
    return output_file_;
  }

  std::vector<std::shared_ptr<arrow::Array>> columns() const {
    return columns_;
  }

  int64_t row_group_size() const { return row_group_size_; }

 private:
  std::shared_ptr<arrow::Schema> schema_;
  std::shared_ptr<arrow::io::FileOutputStream> output_file_;
  std::vector<std::shared_ptr<arrow::Array>> columns_;
  int64_t row_group_size_;
};

}  // namespace parquet_parser
