#include "src/main/cpp/parquet_reader.h"

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <glog/logging.h>

namespace parquet_parser {

ParquetReader::ParquetReader(const std::string &file_path) {
  // init file reader
  std::shared_ptr<arrow::io::ReadableFile> infile;
  PARQUET_ASSIGN_OR_THROW(infile, arrow::io::ReadableFile::Open(
                                      file_path, arrow::default_memory_pool()));

  PARQUET_THROW_NOT_OK(parquet::arrow::OpenFile(
      infile, arrow::default_memory_pool(), &file_reader_));
}

IteratorRange<RowIterator> ParquetReader::Iterate(
    std::vector<int> columns_vec) const {
  RowIterator begin_iterator(columns_vec, 0, file_reader_.get());
  RowIterator end_iterator(columns_vec, file_reader_->num_row_groups(),
                           file_reader_.get());
  return IteratorRange<RowIterator>(begin_iterator, end_iterator);
}

IteratorRange<RowIterator> ParquetReader::IterateRows() const {
  RowIterator begin_iterator(0, file_reader_.get());
  RowIterator end_iterator(file_reader_->num_row_groups(), file_reader_.get());
  return IteratorRange<RowIterator>(begin_iterator, end_iterator);
}

std::vector<std::string> ParquetReader::GetSchemaAsString() const {
  std::shared_ptr<arrow::Schema> schema;
  PARQUET_THROW_NOT_OK(file_reader_->GetSchema(&schema));
  auto fields = schema->fields();
  int field_id = 0;
  std::vector<std::string> res;

  for (auto field : fields) {
    auto data_type = field->type();
    res.push_back(std::to_string(field_id) + ' ' + field->name() + ' ' +
                  data_type->name());
    field_id++;
  }

  return res;
}

std::shared_ptr<arrow::Schema> ParquetReader::GetSchema() const {
  std::shared_ptr<arrow::Schema> schema;
  PARQUET_THROW_NOT_OK(file_reader_->GetSchema(&schema));
  return schema;
}

}  // namespace parquet_parser
