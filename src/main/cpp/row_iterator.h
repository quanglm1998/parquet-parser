#pragma once

#include "src/main/cpp/column_iterator.h"
#include "src/main/cpp/exceptions.h"

namespace parquet_parser {

// Supports iterator for all columns in a row.
// Stores all information of a row group in memory.
class RowIterator {
 public:
  RowIterator() = delete;

  RowIterator(int32_t row_group_id, parquet::arrow::FileReader* reader);

  RowIterator(std::vector<int32_t> columns_vec, int32_t row_group_id,
              parquet::arrow::FileReader* reader);

  RowIterator& operator++(); /**< Pre-increment iterator */

  RowIterator operator++(int); /**< Post-increment iterator */

  bool operator==(const RowIterator& rhs) const;

  bool operator!=(const RowIterator& rhs) const;

  std::vector<std::string> operator*() const;

  std::vector<ColumnIterator<std::string>> column_iterators() const {
    return column_iterators_;
  }

  // get value in a specific column
  template <typename T>
  T Get(int column_id) {
    CHECK(real_column_id_.count(column_id))
        << "This row iterator doesn't contain column " << column_id;
    int real_id = real_column_id_[column_id];
    return FromString<T>(column_iterators_[real_id].GetString());
  }

 private:
  int32_t row_group_id_;
  std::unordered_map<int, int> real_column_id_;
  std::vector<ColumnIterator<std::string>> column_iterators_;
  parquet::arrow::FileReader* reader_;
};

}  // namespace parquet_parser
