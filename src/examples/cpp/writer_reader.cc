#include <glog/logging.h>

#include <random>

#include "src/main/cpp/parquet_reader.h"
#include "src/main/cpp/parquet_writer.h"

static bool ValidateP(const char *flagname, const std::string &value) {
  if (!value.empty()) return true;
  LOG(INFO) << flagname << " can't be empty!";
  return false;
}

DEFINE_string(path, "", "path to parquet file");
DEFINE_validator(path, &ValidateP);

void SetSchema(parquet_parser::ParquetWriter &writer) {
  using parquet_parser::Field;
  using parquet_parser::Type;

  // set schema
  std::vector<Field> fields;
  fields.push_back(Field(Type::BOOL, "bool_col"));

  fields.push_back(Field(Type::INT8, "int8_col"));
  fields.push_back(Field(Type::INT16, "int16_col"));
  fields.push_back(Field(Type::INT32, "int32_col"));
  fields.push_back(Field(Type::INT64, "int64_col"));

  fields.push_back(Field(Type::UINT8, "uint8_col"));
  fields.push_back(Field(Type::UINT16, "uint16_col"));
  fields.push_back(Field(Type::UINT32, "uint32_col"));
  fields.push_back(Field(Type::UINT64, "uint64_col"));

  fields.push_back(Field(Type::FLOAT, "float_col"));
  fields.push_back(Field(Type::DOUBLE, "double_col"));

  fields.push_back(Field(Type::STRING, "string_col"));

  fields.push_back(Field(Type::TIMESTAMP, "timestamp_col"));

  writer.SetSchema(fields);
}

void InsertColumns(parquet_parser::ParquetWriter &writer) {
  size_t seed = 69;
  std::mt19937 rng;
  rng.seed(seed);
  std::uniform_real_distribution<float> float_dis(-100.0, 100.0);
  std::uniform_real_distribution<double> double_dis(-100.0, 100.0);

  std::vector<bool> bool_vec;

  std::vector<int8_t> int8_t_vec;
  std::vector<int16_t> int16_t_vec;
  std::vector<int32_t> int32_t_vec;
  std::vector<int64_t> int64_t_vec;

  std::vector<uint8_t> uint8_t_vec;
  std::vector<uint16_t> uint16_t_vec;
  std::vector<uint32_t> uint32_t_vec;
  std::vector<uint64_t> uint64_t_vec;

  std::vector<float> float_vec;
  std::vector<double> double_vec;

  std::vector<std::string> string_vec;

  std::vector<boost::posix_time::ptime> timestamp_vec;

  for (int i = 0; i < 100; i++) {
    bool_vec.push_back(rng() % 2 == 0);

    int8_t_vec.push_back(rng() % std::numeric_limits<uint8_t>::max());
    int16_t_vec.push_back(rng() % std::numeric_limits<uint16_t>::max());
    int32_t_vec.push_back(rng() % std::numeric_limits<uint32_t>::max());
    int64_t_vec.push_back(rng() * rng() % std::numeric_limits<uint64_t>::max());

    uint8_t_vec.push_back(rng() % std::numeric_limits<uint8_t>::max());
    uint16_t_vec.push_back(rng() % std::numeric_limits<uint16_t>::max());
    uint32_t_vec.push_back(rng() % std::numeric_limits<uint32_t>::max());
    uint64_t_vec.push_back(rng() * rng() %
                           std::numeric_limits<uint64_t>::max());

    float_vec.push_back(float_dis(rng));
    double_vec.push_back(double_dis(rng));

    std::string str = "";
    int len = rng() % 10 + 1;
    for (int i = 0; i < len; i++) {
      str += static_cast<char>(rng() % 26 + 'a');
    }
    string_vec.push_back(str);

    timestamp_vec.push_back(boost::posix_time::from_time_t(
        1618905325 + rng() % (1000 * 60 * 60 * 24)));
  }

  writer.Insert(0, bool_vec);
  writer.Insert(1, int8_t_vec);
  writer.Insert(2, int16_t_vec);
  writer.Insert(3, int32_t_vec);
  writer.Insert(4, int64_t_vec);
  writer.Insert(5, uint8_t_vec);
  writer.Insert(6, uint16_t_vec);
  writer.Insert(7, uint32_t_vec);
  writer.Insert(8, uint64_t_vec);
  writer.Insert(9, float_vec);
  writer.Insert(10, double_vec);
  writer.Insert(11, string_vec);
  writer.Insert(12, timestamp_vec);
}

int main(int argc, char *argv[]) {
  FLAGS_logtostderr = true;

  // Initialize Google's logging library.
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  // WRITE TO FILE

  // declare `writer` with row group size to 1024 bits
  auto writer = parquet_parser::ParquetWriter(FLAGS_path, 1024);
  SetSchema(writer);
  InsertColumns(writer);
  writer.Commit();
  LOG(INFO) << "Wrote to " << FLAGS_path;

  LOG(INFO) << "--------------------------------------------------------------";

  // READ FROM FILE
  auto reader = parquet_parser::ParquetReader(FLAGS_path);

  auto schema = reader.GetSchemaAsString();
  LOG(INFO) << "Opened " << FLAGS_path;
  LOG(INFO) << "--------------------------------------------------------------";

  LOG(INFO) << "Parquet schema:";
  for (auto u : schema) LOG(INFO) << u;
  LOG(INFO) << "--------------------------------------------------------------";

  // get value on a single cell
  // this is a very time-consuming method

  LOG(INFO) << "Single cell : Get<T>";

  LOG(INFO) << "Value in cell (0, 3) is " << reader.Get<int>(0, 3);
  LOG(INFO) << "Value in cell (10, 10) is " << reader.Get<double>(10, 10);
  LOG(INFO) << "Value in cell (20, 11) is " << reader.Get<std::string>(20, 11);
  LOG(INFO) << "Value in cell (50, 12) is "
            << boost::posix_time::to_iso_string(
                   reader.Get<boost::posix_time::ptime>(50, 12));

  LOG(INFO) << "--------------------------------------------------------------";

  // column iteration

  LOG(INFO) << "Column iteration (first 10 elements)";

  int cnt = 0;
  for (auto u : reader.Iterate<int>(3)) {
    LOG(INFO) << "#" << cnt << ": " << u;
    if (++cnt >= 10) break;
  }

  LOG(INFO) << "--------------------------------------------------------------";

  // column iteration with custom parser function

  LOG(INFO) << "Column iteration with custom parser function u -> u + 10 "
               "(first 10 elements)";

  cnt = 0;
  for (auto u : reader.Iterate<int, int>(3, [](int u) { return u + 10; })) {
    LOG(INFO) << "#" << cnt << ": " << u;
    if (++cnt >= 10) break;
  }

  LOG(INFO) << "--------------------------------------------------------------";

  // multiple columns iteration

  LOG(INFO) << "Multiple columns iteration on column 4, 12 (first 10 elements)";

  cnt = 0;
  for (auto u : reader.Iterate({4, 12})) {
    std::string row = u[0] + ", " + u[1];
    LOG(INFO) << row;
    if (++cnt >= 10) break;
  }

  LOG(INFO) << "--------------------------------------------------------------";

  // row iteration

  LOG(INFO) << "Row iteration (first 10 rows)";
  cnt = 0;
  for (auto row : reader.IterateRows()) {
    std::string row_as_string = "";
    for (const std::string &value : row) {
      if (!row_as_string.empty()) row_as_string += ", ";
      row_as_string += value;
    }
    LOG(INFO) << row_as_string;
    if (++cnt >= 10) break;
  }

  LOG(INFO) << "--------------------------------------------------------------";
  return 0;
}
