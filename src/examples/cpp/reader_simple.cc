#include <glog/logging.h>

#include "src/main/cpp/parquet_reader.h"

static bool ValidateP(const char *flagname, const std::string &value) {
  if (!value.empty()) return true;
  LOG(INFO) << flagname << " can't be empty!";
  return false;
}

DEFINE_string(path, "", "path to parquet input file");
DEFINE_validator(path, &ValidateP);

int main(int argc, char *argv[]) {
  FLAGS_logtostderr = true;

  // Initialize Google's logging library.
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  auto reader = parquet_parser::ParquetReader(FLAGS_path);
  LOG(INFO) << "Opened " << FLAGS_path;

  LOG(INFO) << "--------------------------------------------------------------";
  auto schema = reader.GetSchemaAsString();
  LOG(INFO) << "Parquet schema:";
  for (auto u : schema) LOG(INFO) << u;

  LOG(INFO) << "--------------------------------------------------------------";
  LOG(INFO) << "File content:";
  for (const std::vector<std::string> &row : reader.IterateRows()) {
    std::string row_as_string = "";
    for (const std::string &value : row) {
      if (!row_as_string.empty()) row_as_string += ", ";
      row_as_string += value;
    }
    LOG(INFO) << row_as_string;
  }
  return 0;
}
