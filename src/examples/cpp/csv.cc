#include "third_party/csv.hpp"

#include <glog/logging.h>

#include <random>

#include "src/main/cpp/parquet_reader.h"
#include "src/main/cpp/parquet_writer.h"

static bool ValidateP(const char *flagname, const std::string &value) {
  if (!value.empty()) return true;
  LOG(INFO) << flagname << " can't be empty!";
  return false;
}

DEFINE_string(csv_path, "", "path to csv file");
DEFINE_string(parquet_path, "", "path to parquet file");
DEFINE_validator(csv_path, &ValidateP);
DEFINE_validator(parquet_path, &ValidateP);

int main(int argc, char *argv[]) {
  FLAGS_logtostderr = true;

  // Initialize Google's logging library.
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  // Convert to parquet
  using parquet_parser::Field;
  using parquet_parser::Type;

  parquet_parser::ParquetWriter writer(FLAGS_parquet_path);

  // set schema
  std::vector<Field> fields;
  fields.push_back(Field(Type::INT32, "id"));
  fields.push_back(Field(Type::STRING, "name"));

  writer.SetSchema(fields);
  writer.LoadFromCSV(FLAGS_csv_path);
  writer.Commit();

  // Read again
  parquet_parser::ParquetReader reader(FLAGS_parquet_path);


  // Show first 10 rows

  int cnt = 0;
  for (auto row : reader.IterateRows()) {
    std::string row_as_string = "";
    for (const std::string &value : row) {
      if (!row_as_string.empty()) row_as_string += ", ";
      row_as_string += value;
    }
    LOG(INFO) << row_as_string;
    if (++cnt >= 10) break;
  }
  return 0;
}
