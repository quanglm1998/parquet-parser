# Parquet Parser

A C++ library for reading, writing, and analyzing Parquet files.

# Install libs for Ubuntu
  * **arrow**: 

```console
sudo apt update
sudo apt install -y -V ca-certificates lsb-release wget
wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
sudo apt install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
sudo apt update
sudo apt install -y -V libarrow-dev # For C++
sudo apt install -y -V libarrow-glib-dev # For GLib (C)
sudo apt install -y -V libarrow-dataset-dev # For Apache Arrow Dataset C++
sudo apt install -y -V libarrow-flight-dev # For Apache Arrow Flight C++
# Notes for Plasma related packages:
#   * You need to enable "non-free" component on Debian GNU/Linux
#   * You need to enable "multiverse" component on Ubuntu
#   * You can use Plasma related packages only on amd64
sudo apt install -y -V libplasma-dev # For Plasma C++
sudo apt install -y -V libplasma-glib-dev # For Plasma GLib (C)
sudo apt install -y -V libgandiva-dev # For Gandiva C++
sudo apt install -y -V libgandiva-glib-dev # For Gandiva GLib (C)
sudo apt install -y -V libparquet-dev # For Apache Parquet C++
sudo apt install -y -V libparquet-glib-dev # For Apache Parquet GLib (C)
```
  * **boost**:

```console
sudo apt-get install libboost-all-dev
```

# Examples
There are some examples:
  * **reader_simple**: reads a file, then describes its schema and prints out all of its content. To run it, replace {path_file} in the command below.

```console
bazel run src/examples/cpp:reader_simple -- -path {path_file}
```

  * **writer_reader**: uses writer to write to parquet file, then reads it again by reader and perform all iteration methods.

```console
bazel run src/examples/cpp:writer_reader -- -path {path_file}
``` 
  * **csv**: loads data from a csv file and exports it to parquet format.

```console
bazel run src/examples/cpp:csv -- -csv_path {csv_file_path} -parquet_path {parquet_file_path}
```

# Features

## File reader

Passing a string to the constructor of `ParquetReader` causes memory-mapped IO to be used.
Declare *reader* as:

```c++
#include "src/main/cpp/parquet_reader.h"

using namespace parquet_parser;

...

auto reader = ParquetReader(file_path);

...
```

## Get a single table's cell
Getting a cell is performed by calling `reader.Get<T>(row_id, column_id)` with `T` being the type of that column.
**Be careful** when using this feature because when access by `row_id, column_id` randomly, its expected complexity will be `O(num_column)`. 

```c++
...
std::cout << reader.Get<T>(row_id, column_id) << std::endl;
...
```

## Iterate through a column

We can loop through a column without reading the whole big file.
We need to specify the type `T` of the column we want to use.

```c++
#include "src/main/cpp/parquet_reader.h"

using namespace parquet_parser;

...

ParquetReader reader(file_path);

for (T value : reader.Iterate<T>(column_id)) {
  // do something with value
  std::cout << value << std::endl;
}

...
```

We can also iterate with custom parser function.
With `T`, `U` being the type we want and type of the column.
We also need to specify a function that turn a value of type `U` to `T`.


```c++
#include "src/main/cpp/parquet_reader.h"

using namespace parquet_parser;

...

ParquetReader reader(file_path);

for (T value : reader.Iterate<T, U>(column_id, std::function<T(U)>) {
  // do something with value
  std::cout << value << std::endl;
}

...
```

## Dealing with null value
Sometimes we will have `null` value in our table. There're some ways we can deal with this.

### Define default value
We can pass a second parameter as the default value to `Iterate` method of `reader`, `reader` will return that default value if that cell is `null`.

```c++
#include "src/main/cpp/parquet_reader.h"

using namespace parquet_parser;

...

ParquetReader reader(file_path);

// all null values become -1
for (T value : reader.Iterate<T>(column_id, -1)) {
  // do something with value
  std::cout << value << std::endl;
}

...
```

### Optional iteration
Another way to work with null value is to use IterateOptional. Instead of returning type `T` as we want with `Iterate` method, it now returns `std::optional<T>`.

```c++
#include "src/main/cpp/parquet_reader.h"

using namespace parquet_parser;

...

ParquetReader reader(file_path);

for (std::optional<T> value : reader.IterateOptional<T>(column_id)) {
  if (value) {
    // value is not null, work with it normally
  } else {
    // this cell is null
  }
}

...
```

## Multiple columns iteration
Sometimes we want to get multiple columns simultaneously, we could do that by calling `Iterate` with a `std::vector` of column ids. Each row is represented by a vector of strings.

```c++

#include "src/main/cpp/parquet_reader.h"

using namespace parquet_parser;

...

ParquetReader reader(file_path);

for (std::vector<std::string> row : reader.Iterate(columns_vec)) {
  // do something with row
  for (const std::string &value : row) {
    std::cout << value << std::endl;
  }
}
...

```

## Iterate by row

We can loop row by row by calling `IterateRows()` method. Each row is represented by a vector of strings.

```c++

#include "src/main/cpp/parquet_reader.h"

using namespace parquet_parser;

...

ParquetReader reader(file_path);

for (std::vector<std::string> row : reader.IterateRows()) {
  // do something with row
  for (const std::string &value : row) {
    std::cout << value << std::endl;
  }
}
...

```

## Writer
Passing a string to the constructor of `ParquetWriter` causes memory-mapped IO to be used.
All of the cells in the table need to be stored in memory in order to be written to file.
To write to file, we need to go through some step:
  - `SetSchema` lets writer know the parquet table's description (how many columns there are, name and type of each columns).
  - `Insert` a std::vector to a particular column. All columns should have the same number of rows, otherwise the program will crash.
  - `Commit` writes all information to file.

```c++
#include "src/main/cpp/parquet_writer.h"

using namespace parquet_parser;

...

auto reader = ParquetWriter(file_path);

// set schema
std::vector<Field> fields;
fields.push_back(Field(Type::BOOL, "bool_col"));
fields.push_back(Field(Type::INT32, "int32_col"));
fields.push_back(Field(Type::STRING, "string_col"));
fields.push_back(Field(Type::TIMESTAMP, "timestamp_col"));
writer.SetSchema(fields);

// insert columns
writer.Insert(0, bool_vec);
writer.Insert(1, int32_t_vec);
writer.Insert(2, string_vec);
writer.Insert(3, timestamp_vec);

// Write to file
writer.Commit();
...
```

### Insert from CSV
We can also convert from `CSV` files to `parquet`. Instead of inserting every column, call `LoadFromCSV`.

```c++
#include "src/main/cpp/parquet_writer.h"

using namespace parquet_parser;

...

auto reader = ParquetWriter(file_path);

// set schema
std::vector<Field> fields;
fields.push_back(Field(Type::BOOL, "bool_col"));
fields.push_back(Field(Type::INT32, "int32_col"));
fields.push_back(Field(Type::STRING, "string_col"));
fields.push_back(Field(Type::TIMESTAMP, "timestamp_col"));
writer.SetSchema(fields);

// insert columns
writer.LoadFromCSV(csv_file_path);
// Write to file
writer.Commit();
...
```


# Supported types

Parquet parser supports following **C++** types:

  * Numeric types:
    * `bool`
    * `int8_t`
    * `int16_t`
    * `int32_t`
    * `int64_t`
    * `uint8_t`
    * `uint16_t`
    * `uint32_t` (somehow when we try to write `uint32_t` column, it writes `int64_t` instead)
    * `uint64_t`
    * `float`
    * `double`
  * String:
    * `std::string`
  * Timestamp:
    * `boost::posix_time:ptime`
